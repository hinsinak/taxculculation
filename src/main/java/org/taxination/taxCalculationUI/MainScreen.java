package org.taxination.taxCalculationUI;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Frame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;

public class MainScreen extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainScreen frame = new MainScreen();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private void menuBar() {
		// TODO Auto-generated method stub

		JMenuBar menuBar = new JMenuBar();
		menuBar.setToolTipText("");
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmNew = new JMenuItem("New");
		mnFile.add(mntmNew);
		
		JMenuItem mntmSave = new JMenuItem("Save");
		mnFile.add(mntmSave);
		
		JSeparator separator_1 = new JSeparator();
		mnFile.add(separator_1);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mnFile.add(mntmExit);
		
		JMenu mnEmployee = new JMenu("Employee");
		menuBar.add(mnEmployee);
		
		JMenuItem mntmNewEmployee = new JMenuItem("New Employee");
		mnEmployee.add(mntmNewEmployee);
		
		JMenuItem mntmEmployeeList = new JMenuItem("Employee List");
		mnEmployee.add(mntmEmployeeList);
		
		JMenu mnMgt = new JMenu("Mgt");
		menuBar.add(mnMgt);
		
		JMenu mnReport = new JMenu("Report");
		menuBar.add(mnReport);
		
		JMenuItem mntmTaxListReport = new JMenuItem("Tax List Report");
		mnReport.add(mntmTaxListReport);
		
		JMenu mnTaxRule = new JMenu("Tax Rule");
		menuBar.add(mnTaxRule);
		
		JMenuItem mntmTaxRule = new JMenuItem("Tax Rule 2018");
		mnTaxRule.add(mntmTaxRule);
		
		JMenuItem mntmTaxCalculator = new JMenuItem("Tax Calculator");
		mnTaxRule.add(mntmTaxCalculator);
		
		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		JMenuItem mntmWelcome = new JMenuItem("Welcome");
		mnHelp.add(mntmWelcome);
		
		JMenuItem mntmHelpContent = new JMenuItem("Help Content");
		mnHelp.add(mntmHelpContent);
		
		JSeparator separator = new JSeparator();
		mnHelp.add(separator);
		
		JMenuItem mntmCheckForUpdates = new JMenuItem("Check for Updates");
		mnHelp.add(mntmCheckForUpdates);
		
		JMenuItem mntmAboutEmployeeMgt = new JMenuItem("About Employee Mgt");
		mnHelp.add(mntmAboutEmployeeMgt);
	}

	/**
	 * Create the frame.
	 */
	public MainScreen() {
		setExtendedState(Frame.MAXIMIZED_BOTH);
		setTitle("Salary Tax Program v1.0");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		this.menuBar();
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JSplitPane splitPane = new JSplitPane();
		contentPane.add(splitPane, BorderLayout.CENTER);
		splitPane.setDividerLocation(300);
	}

}
